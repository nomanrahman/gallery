(function (window) {
    window.__env = window.__env || {};

    // settings
    window.__env.oauthPersonaAuthenticationUrl = "https://auth-test.logicahealth.org/static/auth/index.html";
    window.__env.defaultServiceUrl = "https://api-test.logicahealth.org/hspc9/data";
    window.__env.baseServiceUrl = "https://api-test.logicahealth.org/";
    window.__env.oauthLogoutUrl = "https://auth-test.logicahealth.org/logout";
    window.__env.userManagementUrl = "https://account.logicahealth.org";
    window.__env.sandboxManagerRESTUrl = "https://sandbox-api.logicahealth.org";
    window.__env.personaCookieDomain = "logicahealth.org";
    window.__env.personaCookieName = "hspc-persona-token";
    window.__env.personaCookieTimeout = 180000;
    window.__env.hspcAccountCookieName = "hspc-token";
    window.__env.firebase = {
        "apiKey": "AIzaSyBHmAKCRdpSMtP53TY7qXjoXPgCniG5T4c",
        "authDomain": "hspc-tst.firebaseapp.com",
        "databaseURL": "https://hspc-tst.firebaseio.com",
        "storageBucket": "hspc-tst.appspot.com"
    }

}(this));
