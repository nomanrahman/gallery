(function (window) {
    window.__env = window.__env || {};

    // settings
    window.__env.oauthPersonaAuthenticationUrl = "https://auth.logicahealth.org/static/auth/index.html";
    window.__env.defaultServiceUrl = "https://api.logicahealth.org/hspc5/data";
    window.__env.baseServiceUrl = "https://api.logicahealth.org/";
    window.__env.oauthLogoutUrl = "https://auth.logicahealth.org/logout";
    window.__env.userManagementUrl = "https://account.logicahealth.org";
    window.__env.sandboxManagerRESTUrl = "https://sandbox-api.logicahealth.org";
    window.__env.personaCookieDomain = "logicahealth.org";
    window.__env.personaCookieName = "hspc-persona-token";
    window.__env.personaCookieTimeout = 180000;
    window.__env.hspcAccountCookieName = "hspc-token";
    window.__env.firebase = {
        "apiKey": "AIzaSyA9Id72GPbTyWbEkyEwkyf1Z_wOJwk0bHc",
        "authDomain": "hspc-3f7ad.firebaseapp.com",
        "databaseURL": "https://hspc-3f7ad.firebaseio.com",
        "storageBucket": "hspc-3f7ad.appspot.com"
    }

}(this));
