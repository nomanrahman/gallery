'use strict';

var env = {};

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}

var ngModule = angular.module('galleryApp', ['ui.router', 'ngSanitize', 'ngAnimate', "ngCookies", 'ui.bootstrap', 'galleryApp.filters', 'galleryApp.services',
    'galleryApp.controllers', 'galleryApp.directives'], function ($stateProvider, $urlRouterProvider) {


    $urlRouterProvider.otherwise('/app-gallery');
    //$urlRouterProvider.otherwise('/coming-soon');

    $stateProvider

        .state('coming-soon', {
            url: '/coming-soon',
            templateUrl: 'js/templates/coming-soon.html'
        })

        .state('login', {
            url: '/login',
            templateUrl: 'js/templates/login.html'
        })

        .state('app-gallery', {
            url: '/app-gallery',
            templateUrl: 'js/templates/appsGallery.html'
        })

        .state('after-auth', {
            url: '/after-auth',
            templateUrl: 'js/templates/after-auth.html'
        })
});

// Register environment in AngularJS as constant
ngModule.constant('__env', env);
