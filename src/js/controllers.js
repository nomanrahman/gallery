'use strict';

angular
    .module('galleryApp.controllers', [])
    .controller('navController', [
        "$rootScope", "$scope", "__env", "fhirApiServices", "userServices", "oauth2", "$location", "$state", "termsOfUseService", "$uibModal", "cookieService", "$window",
        function ($rootScope, $scope, __env, fhirApiServices, userServices, oauth2, $location, $state, termsOfUseService, $uibModal, cookieService, $window) {

            $scope.showing = {
                signout: false,
                signin: true,
                loading: false,
                apps: true,
                navBar: false
            };
            $scope.messages = [];

            $rootScope.$on('message-notify', function (event, messages) {
                $scope.messages = messages;
                $rootScope.$digest();
            });

            $scope.signin = function () {
                $state.go('login', {});
            };

            $rootScope.$on('signed-in', function (event, arg) {
                userServices.getOAuthUserFromServer().then(function () {
                    $scope.oauthUser = userServices.getOAuthUser();
                    userServices.getSandboxManagerUser($scope.oauthUser.sbmUserId).then(function (sandboxManagerUser) {
                        if (sandboxManagerUser === undefined || sandboxManagerUser === "") {
                            $scope.signout();
                        } else if (sandboxManagerUser.hasAcceptedLatestTermsOfUse === false) {
                            termsOfUseService.getTermsOfUse().then(function (terms) {
                                var modalInstance = $uibModal.open({
                                    animation: true,
                                    templateUrl: 'js/templates/termsOfUseModal.html',
                                    controller: 'TermsOfUseModalInstanceCtrl',
                                    windowClass: 'terms-modal-window',
                                    resolve: {
                                        getSettings: function () {
                                            return {
                                                title: "Terms of Use and Privacy Statement",
                                                ok: "Accept",
                                                cancel: "Decline",
                                                showAccept: true,
                                                isUpdate: (sandboxManagerUser.termsOfUseAcceptances.length > 0),
                                                text: terms.value,
                                                callback: function (result) { //setting callback
                                                    if (result == true) {
                                                        termsOfUseService.acceptTermsOfUse($scope.oauthUser.sbmUserId, terms.id);
                                                    } else {
                                                        $scope.signout();
                                                    }
                                                }
                                            };
                                        }
                                    }
                                });
                                modalInstance.result.then(function (result) {
                                }, function () {
                                    $scope.signout();
                                });
                            });

                        }
                    });

                    $scope.showing.signin = false;
                    $scope.showing.signout = true;
                    $scope.showing.navBar = true;
                    $rootScope.$digest();
                    $state.go('app-gallery', {});
                });

            });

            $rootScope.$on('hide-nav', function () {
                $scope.showing.navBar = false;
            });

            $scope.signout = function () {
                fhirApiServices.clearClient();
                oauth2.logout();
            };

            $scope.userSettings = function () {
                $window.open(__env.userManagementUrl, '_blank');
            };

            $scope.$on('$viewContentLoaded', function () {
                if (fhirApiServices.clientInitialized()) {
                    // $rootScope.$emit('signed-in');
                } else if (sessionStorage.tokenResponse) {
                    fhirApiServices.initClient();
                } else if (window.location.hash.indexOf("#/after-auth") !== 0) {
                    if (cookieService.getHSPCAccountCookie()) {
                        oauth2.login();
                    }
                }
            });

            $scope.showTerms = function () {
                termsOfUseService.getTermsOfUse().then(function (terms) {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'js/templates/termsOfUseModal.html',
                        controller: 'TermsOfUseModalInstanceCtrl',
                        windowClass: 'terms-modal-window',
                        resolve: {
                            getSettings: function () {
                                return {
                                    title: "Terms of Use and Privacy Statement",
                                    cancel: "Close",
                                    showAccept: false,
                                    text: terms.value,
                                    callback: function (result) { //setting callback
                                    }

                                };
                            }
                        }
                    });
                });
            };

        }])
    .controller("AfterAuthController", // After auth
        function (fhirApiServices) {
            fhirApiServices.initClient();
        })
    .controller("LoginController",
        function ($rootScope, oauth2, fhirApiServices) {

            if (fhirApiServices.clientInitialized()) {
                // $rootScope.$emit('signed-in');
            } else {
                oauth2.login();
            }

        })
    .controller("AppsGalleryController", function ($scope, apps, userServices, fhirApiServices, launchApp, $uibModal) {
        $scope.authenticated = typeof fhirApiServices.fhirClient() !== "undefined";
        $scope.showing.navBar = true;
        $scope.all_user_apps = [];

        $scope.compareApps = function (a, b) {
            var aPriority =
                (a === undefined
                    ? Number.MAX_SAFE_INTEGER
                    : (a.hasOwnProperty("priority") && a.priority !== undefined)
                        ? a.priority
                        : Number.MAX_SAFE_INTEGER);
            var bPriority =
                (b === undefined
                    ? Number.MAX_SAFE_INTEGER
                    : (b.hasOwnProperty("priority") && b.priority !== undefined)
                        ? b.priority
                        : Number.MAX_SAFE_INTEGER);

            return aPriority - bPriority;
        }

        apps.getGalleryApps().then(function (firebaseApps) {
            angular.forEach(firebaseApps, function (firebaseApp) {
                apps.loadManifestInfo(firebaseApp).then(function () {
                    if(firebaseApp.healthy == "true") {
                      $scope.all_user_apps.push(firebaseApp);
                      // can't figure out a better way than to sort these after each push
                      $scope.all_user_apps.sort($scope.compareApps);
                    }
                  else {
                      console.log(firebaseApp.name + " is not healthy");
                  }
                });
            });
        });

        $scope.info = function (app) {
            $scope.modalOpen = true;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'js/templates/infoModal.html',
                controller: 'InfoModalInstanceCtrl',
                size: 'lg',
                resolve: {
                    getApp: function () {
                        return {
                            app: app
                        }
                    },
                    getAuthenticated: function () {
                        return {
                            authenticated: $scope.authenticated
                        }
                    }
                }
            });

            modalInstance.result.then(function (app) {
                $scope.launch(app)
            }, function () {
            });

        };

        $scope.launch = function (app) {
            launchApp.launch(app, app.patient_search, app.contextParams, app.persona);
        };

    })
    .controller('InfoModalInstanceCtrl', ['$scope', '$uibModalInstance', 'getApp', 'getAuthenticated',
        function ($scope, $uibModalInstance, getApp, getAuthenticated) {

            $scope.app = getApp.app;
            $scope.authenticated = getAuthenticated.authenticated;

            $scope.launch = function (app) {
                $uibModalInstance.close(app);
            };

            $scope.close = function () {
                $uibModalInstance.dismiss();
            };
        }]
    )
    .controller('TermsOfUseModalInstanceCtrl', ['$scope', '$uibModalInstance', 'getSettings',
        function ($scope, $uibModalInstance, getSettings) {

            $scope.title = (getSettings.title !== undefined) ? getSettings.title : "";
            $scope.ok = (getSettings.ok !== undefined) ? getSettings.ok : "Yes";
            $scope.cancel = (getSettings.cancel !== undefined) ? getSettings.cancel : "No";
            $scope.text = (getSettings.text !== undefined) ? getSettings.text : "Continue?";
            $scope.showAccept = (getSettings.showAccept !== undefined) ? getSettings.showAccept : false;
            $scope.isUpdate = (getSettings.isUpdate !== undefined) ? getSettings.isUpdate : false;
            var callback = (getSettings.callback !== undefined) ? getSettings.callback : null;

            $scope.openPDF = function () {
                window.open('https://content.logicahealth.org/docs/hspc/privacyterms.pdf', '_blank');
            };

            $scope.confirm = function (result) {
                $uibModalInstance.close(result);
                callback(result);
            };
        }]
    )
;

