'use strict';

angular.module('galleryApp.services', [])
    .factory('oauth2', ['$rootScope', '$location', '__env', 'cookieService', function ($rootScope, $location, __env, cookieService) {

        var authorizing = false;

        return {
            authorizing: function () {
                return authorizing;
            },
            authorize: function () {
                // If the persona auth was cut short, it's possible that the persona cookie remains in the browser. Since
                // the sandbox manager should never be accessed as persona user, we can use this as a fail-safe to
                // delete any errant persona auth cookies.
                cookieService.removePersonaCookieIfExists();

                // window.location.origin does not exist in some non-webkit browsers
                if (!window.location.origin) {
                    window.location.origin = window.location.protocol + "//"
                        + window.location.hostname
                        + (window.location.port ? ':' + window.location.port : '');
                }

                var thisUri = window.location.origin + window.location.pathname;
                var thisUrl = thisUri.replace(/\/+$/, "/");

                var client = {
                    "client_id": "hspc_gallery",
                    "redirect_uri": thisUrl,
                    "scope": "smart/orchestrate_launch user/*.* profile openid"
                };
                authorizing = true;

                FHIR.oauth2.authorize({
                    client: client,
                    server: __env.defaultServiceUrl,
                    from: $location.url()
                }, function (err) {
                    authorizing = false;
                    $rootScope.$emit('error', err);
//                    $rootScope.$emit('set-loading');
//                    $rootScope.$emit('clear-client');
//                    var loc = "/ui/select-patient";
//                    if ($location.url() !== loc) {
//                        $location.url(loc);
//                    }
//                    $rootScope.$digest();
                });
            },
            logout: function () {
                window.location.href = __env.oauthLogoutUrl
                    + "?hspcRedirectUrl="
                    + encodeURI(__env.baseUrl);
            },
            login: function (sandboxId) {
                var that = this;
                that.authorize();
            }
        };
    }]).factory('fhirApiServices', ['oauth2', '__env', 'notification', '$rootScope', '$location', function (oauth2, __env, notification, $rootScope, $location) {

    /**
     *
     *      FHIR SERVICE API CALLS
     *
     **/

    var fhirClient;

    function getQueryParams(url) {
        var index = url.lastIndexOf('?');
        if (index > -1) {
            url = url.substring(index + 1);
        }
        var urlParams;
        var match,
            pl = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) {
                return decodeURIComponent(s.replace(pl, " "));
            },
            query = url;

        urlParams = {};
        while (match = search.exec(query))
            urlParams[decode(match[1])] = decode(match[2]);
        return urlParams;
    }

    return {
        clearClient: function () {
            fhirClient = null;
            sessionStorage.clear();
        },
        fhirClient: function () {
            return fhirClient;
        },
        clientInitialized: function () {
            return (fhirClient !== undefined && fhirClient !== null);
        },
        initClient: function () {
            var params = getQueryParams($location.url());
            if (params.code) {
                delete sessionStorage.tokenResponse;
                FHIR.oauth2.ready(params, function (newSmart) {
                    if (newSmart && newSmart.state && newSmart.state.from !== undefined) {
                        $location.url(newSmart.state.from);
                        fhirClient = newSmart;
                        $rootScope.$emit('signed-in');
                        $rootScope.$digest();
                    }
                });
            } else {
                oauth2.login();
            }
        },
        registerContext: function (app, params, issuer) {
            var deferred = $.Deferred();

            var reqLaunch = fhirClient.authenticated({
                url: issuer + '/_services/smart/Launch',
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify({
                    client_id: app.clientId,
                    parameters: params
                })
            });

            $.ajax(reqLaunch)
                .done(deferred.resolve)
                .fail(deferred.reject);
            return deferred;
        }
    }
}]).factory('userServices', ['$rootScope', 'fhirApiServices', '$filter', '__env', function ($rootScope, fhirApiServices, $filter, __env) {
    var oauthUser;
    var sandboxManagerUser;

    return {
        getOAuthUser: function () {
            return oauthUser;
        },
        getOAuthUserFromServer: function () {
            var deferred = $.Deferred();
            if (oauthUser !== undefined) {
                deferred.resolve(oauthUser);
            } else {
                var userInfoEndpoint = fhirApiServices.fhirClient().state.provider.oauth2.authorize_uri.replace("authorize", "userinfo");
                $.ajax({
                    url: userInfoEndpoint,
                    type: 'GET',
                    contentType: "application/json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + fhirApiServices.fhirClient().server.auth.token);
                    }
                }).done(function (result) {
                    oauthUser = {
                        sbmUserId: result.sub,
                        name: result.name,
                        email: result.preferred_username
                    };
                    deferred.resolve(oauthUser);
                }).fail(function () {
                });
            }
            return deferred;
        },
        getSandboxManagerUser: function (sbmUserId) {
            var deferred = $.Deferred();
            if (sandboxManagerUser !== undefined) {
                deferred.resolve(sandboxManagerUser);
            } else {
                $.ajax({
                    url: __env.sandboxManagerRESTUrl + "/user?sbmUserId=" + sbmUserId,
                    type: 'GET',
                    contentType: "application/json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + fhirApiServices.fhirClient().server.auth.token);
                    }
                }).done(function (userResult) {
                    sandboxManagerUser = userResult;
                    deferred.resolve(userResult);
                }).fail(function (error) {
                    deferred.reject(error);
                });
            }
            return deferred;
        }
    };
}]).factory('termsOfUseService', ['fhirApiServices', '__env', function (fhirApiServices, __env) {
    return {
        getTermsOfUse: function () {
            var deferred = $.Deferred();
            $.ajax({
                url: "https://content.logicahealth.org/docs/hspc/privacyterms.html",
                type: 'GET',
                contentType: "application/json"
            }).done(function (terms) {
                deferred.resolve(terms);
            }).fail(function (error) {
                deferred.reject(error);
            });
            return deferred;
        },
        acceptTermsOfUse: function (sbmUserId, termsId) {
            var deferred = $.Deferred();
            var url = __env.sandboxManagerRESTUrl + "/user/acceptterms?sbmUserId=" + encodeURIComponent(sbmUserId) + "&termsId=" + termsId;
            $.ajax({
                url: url,
                type: 'POST',
                contentType: "application/json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + fhirApiServices.fhirClient().server.auth.token);
                }
            }).done(function (terms) {
                deferred.resolve(terms);
            }).fail(function (error) {
                deferred.reject(error);
            });
            return deferred;
        }
    };
}]).factory('launchApp', ['$rootScope', 'fhirApiServices', 'apps', 'random', '__env', '$http', '$log', 'cookieService', function ($rootScope, fhirApiServices, apps, random, __env, $http, $log, cookieService) {

    var appWindow;

    function registerAppContext(app, params, key) {
        var appToLaunch = angular.copy(app);
        delete appToLaunch.clientJSON;

        callRegisterContext(appToLaunch, params, fhirApiServices.fhirClient().server.serviceUrl, key);
    }

    function callRegisterContext(appToLaunch, params, issuer, key) {
        fhirApiServices
            .registerContext(appToLaunch, params, issuer)
            .done(function (c) {
                window.localStorage[key] = JSON.stringify({
                    app: appToLaunch,
                    iss: issuer,
                    context: c
                });
            }).fail(function (err) {
            console.log("Could not register launch context: ", err);
            appWindow.close();
            $rootScope.$emit('error', 'Could not register launch context (see console)');
            $rootScope.$digest();
        });
    }

    return {
        /* Hack to get around the window popup behavior in modern web browsers
         (The window.open needs to be synchronous with the click event to
         avoid triggering  popup blockers. */

        launch: function (app, patientContext, contextParams, userPersona) {
            var key = random(32);
            window.localStorage[key] = "requested-launch";

            var params = {};
            if (patientContext !== undefined && patientContext !== "") {
                params = {patient: patientContext}
            }

            if (contextParams !== undefined) {
                for (var i = 0; i < contextParams.length; i++) {
                    params[contextParams[i].name] = contextParams[i].value;
                }
            }

            appWindow = window.open('launchGalleryApp.html?' + key, '_blank');
            registerAppContext(app, params, key);
            if (userPersona !== null && userPersona !== undefined && userPersona) {
                $http.post(__env.sandboxManagerRESTUrl + "/userPersona/authenticate", {
                    "username": userPersona.personaUserId,
                    "password": userPersona.password
                }).then(function (response) {
                    cookieService.addPersonaCookie(response.data.jwt);
                }).catch(function (error) {
                    $log.error(error);
                    appWindow.close();
                });
            }
        }
    }

}]).factory('random', function () {
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return function randomString(length) {
        var result = '';
        for (var i = length; i > 0; --i) {
            result += chars[Math.round(Math.random() * (chars.length - 1))];
        }
        return result;
    }
}).factory('tools', function () {

    return {
        decodeURLParam: function (url, param) {
            var query;
            var data;
            var result = [];

            try {
                query = decodeURIComponent(url).split("?")[1];
                data = query.split("&");
            } catch (err) {
                return null;
            }

            for (var i = 0; i < data.length; i++) {
                var item = data[i].split("=");
                if (item[0] === param) {
                    result.push(item[1]);
                }
            }

            if (result.length === 0) {
                return null;
            }
            return result[0];
        }
    };

}).factory('notification', ['$rootScope', function ($rootScope) {
    var messages = [];

    return {
        message: function (message) {
            messages = messages.filter(function (obj) {
                return (obj.isVisible !== false );
            });

            var finalMessage;
            if (message.text === undefined) {
                finalMessage = {
                    type: 'message',
                    text: message
                }
            } else {
                finalMessage = message;
            }
            messages.push(finalMessage);
            $rootScope.$emit('message-notify', messages);
        },
        messages: function () {
            return messages;
        }
    }
}]).factory('apps', ['$http', '__env', function ($http, __env) {

    return {
        getGalleryApps: function () {
            let config = {
                apiKey: __env.firebase.apiKey,
                authDomain: __env.firebase.authDomain,
                databaseURL: __env.firebase.databaseURL,
                storageBucket: __env.firebase.storageBucket
            };
            firebase.initializeApp(config);

            return firebase.database().ref('/gallery').orderByChild("priority").once('value').then(function (gallerySnapshot) {
                return gallerySnapshot.val().apps;
            });
        },
        loadManifestInfo: function (firebaseApp) {
            return $http.get(firebaseApp.manifest_url).then(function (response) {
                firebaseApp.manifestInfo = response.data;
            });
        }
    };
}]).factory('cookieService', ['$cookies', '__env', '$log', function ($cookies, __env, $log) {
    return {
        addPersonaCookie: function (cookieJwtValue) {
            $cookies.put(__env.personaCookieName, cookieJwtValue, {
                path: "/",
                domain: __env.personaCookieDomain,
                expires: new Date((new Date()).getTime() + __env.personaCookieTimeout)
            });
        },
        removePersonaCookieIfExists: function () {
            if ($cookies.get(__env.personaCookieName))
                $cookies.remove(__env.personaCookieName,
                    {
                        path: "/",
                        domain: __env.personaCookieDomain
                    });
        },
        getHSPCAccountCookie: function () {
            return $cookies.get(__env.hspcAccountCookieName);
        }
    }
}]);
